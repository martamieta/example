
def len_of_queues(queues, value):
    """Finds number of queues with desired size

    :type queues: list of queues
    :type value: int
    :rtype: int
    """

    return len([item for item in queues if item.details['exact_size'] == value])


def compare_macs(mac1, mac2):
    """Compares different formats of MAC addresses

    :param mac1: Mac address
    :type mac1: str
    :param mac2: Mac address
    :type mac2: str
    :return: True if mac addresses are the same
    :rtype: bool
    """
    separators = [":", "-"]

    for separator in separators:
        if separator in mac1:
            mac1 = mac1.replace(separator, "").upper()
        if separator in mac2:
            mac2 = mac2.replace(separator, "").upper()

    if mac1 == mac2:
        return True
    return False
